/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.knotek.adrapp.util;

import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 *
 * @author david
 */
public class AlertBox {

    private AlertBox() {
    }

    /**
     * Dialogové okno typu OK.
     * @param title titulek okna
     * @param header hlavicka okna
     * @param content obsah okna
     * @param event udalost
     * @param alertType typ dialogoveho okna
     */
    public static void displayOK(String title, String header, String content, ActionEvent event, Alert.AlertType alertType) {
        Node node = (Node) event.getSource();
        AlertBox.displayOK(title, header, content, node, alertType);
    }
    
    /**
     * Dialogové okno typu OK.
     * @param title titulek okna
     * @param header hlavicka okna
     * @param content obsah okna
     * @param node tlacitko, ktere okno vyvolalo
     * @param alertType typ dialogoveho okna
     */
    public static void displayOK(String title, String header, String content, Node node, Alert.AlertType alertType) {
        if (alertType.equals(Alert.AlertType.CONFIRMATION)) {
            alertType = Alert.AlertType.INFORMATION;
        }
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        
        Label label = new Label(content);
        label.setWrapText(true);
        alert.getDialogPane().setContent(label);
        //alert.setContentText(content);
        
        alert.initOwner((Stage) node.getScene().getWindow());
        alert.showAndWait();
    }
    
    /**
     * Dialogové okno typu Ano/Ne.
     * @param title titulek okna
     * @param header hlavicka okna
     * @param content obsah okna
     * @param event udalost
     * @return true nebo false podle stisknuteho tlaacitka
     */
    public static boolean displayYN(String title, String header, String content, ActionEvent event) {
        Node node = (Node) event.getSource();
        return displayYN(title, header, content, node);
    }
    
    /**
     * Dialogové okno typu Ano/Ne.
     * @param title titulek okna
     * @param header hlavicka okna
     * @param content obsah okna
     * @param node tlacitko, ktere okno vyvolalo
     * @return true nebo false podle stisknuteho tlaacitka
     */
    public static boolean displayYN(String title, String header, String content, Node node) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        
        Label label = new Label(content);
        label.setWrapText(true);
        alert.getDialogPane().setContent(label);
        //alert.setContentText(content);
        
        alert.initOwner((Stage) node.getScene().getWindow());
        
        ButtonType buttonYes = new ButtonType("Ano", ButtonBar.ButtonData.YES);
        ButtonType buttonNo = new ButtonType("Ne", ButtonBar.ButtonData.NO);
        alert.getButtonTypes().setAll(buttonNo, buttonYes);
        
        Optional<ButtonType> result = alert.showAndWait();
        
        return result.get() == buttonYes;
    }
}
