package com.knotek.adrapp.adresar.view;

import com.knotek.adrapp.adresar.controller.Adresar;
import com.knotek.adrapp.adresar.model.AdresarCsv;
import com.knotek.adrapp.adresar.model.Osoba;
import com.knotek.adrapp.adresar.model.AdresarMysql;
import com.knotek.adrapp.adresar.model.AdresarSqlite;
import com.knotek.adrapp.adresar.export.TypExport;
import com.knotek.adrapp.adresar.model.TypHledat;
import com.knotek.adrapp.util.AlertBox;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class Kontroler patřící k oknu adresar.fxml. Díky tomuto
 * kontroleru můžeme pracovat s jednotlivými prvky v okně.
 */
public class AdresarController implements Initializable {

    private Adresar adresar;
    private Osoba vybranaOsoba;
    private String typDatabaze;

    @FXML
    private TableView<Osoba> tvOsoby;

    @FXML
    private TableColumn<Osoba, String> tcJmeno;

    @FXML
    private TableColumn<Osoba, String> tcTelefon;

    @FXML
    private TextField tfJmeno;

    @FXML
    private TextField tfPrijmeni;

    @FXML
    private TextField tfEmail;

    @FXML
    private TextField tfTelefon;

    @FXML
    private ComboBox<String> cbxTypHledat;

    @FXML
    private TextField tfHledat;

    @FXML
    private Button btnEditovat;

    @FXML
    private Button btnSmazat;

    public AdresarController() {
        adresar = new Adresar(new AdresarSqlite());
        typDatabaze = "SQLite";
        vybranaOsoba = null;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Prováže sloupce tabulky TableView s proměnnou instance třídy Osoba.
        tcJmeno.setCellValueFactory(data -> data.getValue().prijmeniJmenoProperty());
        tcTelefon.setCellValueFactory(data -> data.getValue().telefonProperty());

        // Vložení seznamu hodnot do ComboBoxu (podle čeho se budou osoby vyhledávat).
        cbxTypHledat.getItems().setAll(TypHledat.JMENO.get(), TypHledat.PRIJMENI.get(), TypHledat.EMAIL.get(), TypHledat.TELEFON.get());
        cbxTypHledat.getSelectionModel().selectFirst();

        // Reakce na vybrání osoby v tabulce TableView
        tvOsoby.getSelectionModel().selectedItemProperty().addListener((hodnota, staraH, novaH) -> zobrazitOsobu(novaH));

        tvOsoby.setItems(adresar.zobrazitVsechnyOsoby());
    }

    /**
     * Po vybráni osoby zobrazíme detail osoby v prvcích našeho okna.
     *
     * @param osoba
     */
    private void zobrazitOsobu(Osoba osoba) {
        if (osoba != null) {
            vybranaOsoba = osoba;
            tfJmeno.setText(osoba.getJmeno());
            tfPrijmeni.setText(osoba.getPrijmeni());
            tfEmail.setText(osoba.getEmail());
            tfTelefon.setText(osoba.getTelefon());
        } else {
            tfJmeno.setText("");
            tfPrijmeni.setText("");
            tfEmail.setText("");
            tfTelefon.setText("");
        }
    }

    /**
     * Zobrazí okno pro editaci osoby. Do okna vloží položky vybrané osoby.
     *
     * @param event
     */
    @FXML
    private void onEditovatOsobu(ActionEvent event) {
        if (vybranaOsoba != null) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/knotek/adrapp/adresar/view/nova_osoba.fxml"));
                Parent root = (Parent) loader.load();
                NovaOsobaController controller = loader.getController(); // přístup ke kontroleru okna

                Stage stage = new Stage();
                stage.setTitle("Editovat osobu");
                stage.setScene(new Scene(root));

                stage.setMinWidth(330.0);
                stage.setMinHeight(260.0);
                stage.setMaxWidth(330.0);
                stage.setMaxHeight(260.0);

                // Aby se nám okno neschovávalo pod okno, které toto okno vyvolalo
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initOwner(((Node) event.getSource()).getScene().getWindow());

                // odešleme vybranou osobu, jenž v okně zobrazíme
                controller.setOsoba(vybranaOsoba);

                stage.showAndWait();

                // Pokud jsem v okně stiskli Uložit, uložíme nová data do databáze
                if (controller.isOK()) {
                    adresar.editovatOsobu(vybranaOsoba.getId(), controller.getOsoba());
                    tvOsoby.setItems(adresar.zobrazitVsechnyOsoby());
                }

            } catch (IOException ex) {
                Logger.getLogger(AdresarController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            AlertBox.displayOK("Editovat osobu",
                    "Nelze editovat osobu",
                    "Nejdříve vyberte osobu, kterou chcete editovat.",
                    event,
                    Alert.AlertType.INFORMATION);
        }
    }

    /**
     * Vložíme novou osobu
     *
     * @param event
     */
    @FXML
    private void onNovaOsoba(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/knotek/adrapp/adresar/view/nova_osoba.fxml"));
            Parent root = (Parent) loader.load();
            NovaOsobaController controller = loader.getController();

            Stage stage = new Stage();
            stage.setTitle("Nová osoba");
            stage.setScene(new Scene(root));

            // Aby se nám okno neschovávalo pod okno, které toto okno vyvolalo
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(((Node) event.getSource()).getScene().getWindow());

            stage.setMinWidth(330.0);
            stage.setMinHeight(260.0);
            stage.setMaxWidth(330.0);
            stage.setMaxHeight(260.0);

            stage.showAndWait();

            // Pokud jsem v okně stiskli Uložit, uložíme nová data do databáze
            if (controller.isOK()) {
                adresar.vlozitNovouOsobu(controller.getOsoba());
                tvOsoby.setItems(adresar.zobrazitVsechnyOsoby());
            }

        } catch (IOException ex) {
            Logger.getLogger(AdresarController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Odstraníme osobu
     *
     * @param event
     */
    @FXML
    private void onSmazatOsobu(ActionEvent event) {
        if (vybranaOsoba != null) {
            if (AlertBox.displayYN("Odstranit osobu",
                    "Opravdu chcete odstranit osobu: " + vybranaOsoba.getJmenoPrijmeni() + "?",
                    "Tento krok již nepůjde vzít zpět",
                    event)) {
                adresar.odstranitOsobu(vybranaOsoba.getId());
                tvOsoby.setItems(adresar.zobrazitVsechnyOsoby());
                vybranaOsoba = null;
            }
        } else {
            AlertBox.displayOK("Smazat osobu",
                    "Nelze odstranit osobu.",
                    "Nejdříve vyberte osobu, kterou chcete odstranit.",
                    event, Alert.AlertType.INFORMATION);
        }
    }

    /**
     * Vyhledat osobu
     *
     * @param event
     */
    @FXML
    private void onHledat(ActionEvent event) {
        TypHledat typHledat;

        // Z ComboBoxu zjistíme podle čeho chceme hledat
        if (cbxTypHledat.getValue().equals(TypHledat.JMENO.get())) {
            typHledat = TypHledat.JMENO;
        } else if (cbxTypHledat.getValue().equals(TypHledat.PRIJMENI.get())) {
            typHledat = TypHledat.PRIJMENI;
        } else if (cbxTypHledat.getValue().equals(TypHledat.EMAIL.get())) {
            typHledat = TypHledat.EMAIL;
        } else if (cbxTypHledat.getValue().equals(TypHledat.TELEFON.get())) {
            typHledat = TypHledat.TELEFON;
        } else {
            typHledat = TypHledat.JMENO;
        }

        tvOsoby.setItems(adresar.vyhledatOsoby(typHledat, tfHledat.getText()));
    }

    /**
     * Přepneme databázi na SQLite a data zobrazíme v tabulce
     */
    public void prepnoutNaSqlite() {
        adresar = new Adresar(new AdresarSqlite());
        tvOsoby.setItems(adresar.zobrazitVsechnyOsoby());
        typDatabaze = "SQLite";
        vybranaOsoba = null;
    }

    /**
     * Přepneme databázi na MySql a data zobrazíme v tabulce
     */
    public void prepnoutNaMysql() {
        adresar = new Adresar(new AdresarMysql());
        tvOsoby.setItems(adresar.zobrazitVsechnyOsoby());
        typDatabaze = "MySql";
        vybranaOsoba = null;
    }

    /**
     * Přepneme na CSV a data zobrazíme v tabulce
     */
    public void prepnoutNaCsv() {
        adresar = new Adresar(new AdresarCsv());
        tvOsoby.setItems(adresar.zobrazitVsechnyOsoby());
        vybranaOsoba = null;
        typDatabaze = "CSV";
    }

    /**
     * exportujeme osoby do csv
     *
     * @param nazevSouboru nazev souboru
     * @param exportTyp typ exportu
     */
    public void exportovatOsoby(String nazevSouboru, TypExport exportTyp) {
        if (adresar.exportovatOsoby(nazevSouboru, exportTyp)) {
            AlertBox.displayOK("Exportovat osoby", "Exportování osob proběhlo v pořádku", "", tvOsoby, Alert.AlertType.INFORMATION);
        } else {
            AlertBox.displayOK("Exportovat osoby", "Exportování osob se nezdařilo", "", tvOsoby, Alert.AlertType.ERROR);
        }
    }

    /**
     * Typ databáze, který máme zobrazený.
     *
     * @return řetězec vybrané databáze
     */
    public String getTypDatabaze() {
        return typDatabaze;
    }
}
