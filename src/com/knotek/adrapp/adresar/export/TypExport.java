package com.knotek.adrapp.adresar.export;

/**
 * Typ exportu (csv, xml). Řetězce se použijí v comboboxu.
 */
public enum TypExport {
    CSV("CSV"), XML("XML");

    String typ;

    private TypExport(String typ) {
        this.typ = typ;
    }

    public String get() {
        return typ;
    }

}
