package com.knotek.adrapp.adresar.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Třída je přímým potomkem abstraktní třídy AdresarSql, a dědí vše, včetně přihlášení se 
 * k interface AdresarData. Abstraktní třída AdresarSql nedefinovala jednu metodu (connect()),
 * která musí být definována zde.
 */
public class AdresarMysql extends AdresarSql {
    
    /**
     * Konstruktor, který volá konstruktor třídy svého rodiče a říká mu, 
     * jaký zápis AUTO_INCREMENT má použít.
     */
    public AdresarMysql() {
        super(TypIncrement.MYSQL);
    }
    
    /**
     * Pripojeni k databazi mysql
     */
    @Override
    public void connect() {
        Connection conn;
        String url = "jdbc:mysql://192.168.1.210:3307/adresar";
        String user = "david";
        String password = "notebook*DELL";
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, password);
            setConnection(conn);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AdresarMysql.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Nepodarilo se pripojit k databazovemu serveru.");
        }
    }

}
