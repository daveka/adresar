package com.knotek.adrapp.adresar.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Objekty této třídy budou uchovávat potřebné informace o osobě. Je doporučeno
 * používat property.
 */
public class Osoba {

    private final IntegerProperty id;
    private final StringProperty jmeno;
    private final StringProperty prijmeni;
    private final StringProperty email;
    private final StringProperty telefon;

    private final StringProperty jmenoPrijmeni;
    private final StringProperty prijmeniJmeno;

    public Osoba(int id, String jmeno, String prijmeni, String email, String telefon) {
        this.id = new SimpleIntegerProperty(id);
        this.jmeno = new SimpleStringProperty(jmeno);
        this.prijmeni = new SimpleStringProperty(prijmeni);
        this.email = new SimpleStringProperty(email);
        this.telefon = new SimpleStringProperty(telefon);

        this.jmenoPrijmeni = new SimpleStringProperty(jmeno + " " + prijmeni);
        this.prijmeniJmeno = new SimpleStringProperty(prijmeni + " " + jmeno);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty jmenoProperty() {
        return jmeno;
    }

    public StringProperty prijmeniProperty() {
        return prijmeni;
    }

    public StringProperty emailProperty() {
        return email;
    }

    public StringProperty telefonProperty() {
        return telefon;
    }

    public StringProperty jmenoPrijmeniProperty() {
        return jmenoPrijmeni;
    }

    public StringProperty prijmeniJmenoProperty() {
        return prijmeniJmeno;
    }

    public int getId() {
        return id.get();
    }

    public String getJmeno() {
        return jmeno.get();
    }

    public String getPrijmeni() {
        return prijmeni.get();
    }

    public String getEmail() {
        return email.get();
    }

    public String getTelefon() {
        return telefon.get();
    }

    public String getJmenoPrijmeni() {
        return jmenoPrijmeni.get();
    }

    public String getPrijmeniJmeno() {
        return prijmeniJmeno.get();
    }
}
