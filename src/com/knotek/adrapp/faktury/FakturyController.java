package com.knotek.adrapp.faktury;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

public class FakturyController implements Initializable {

    @FXML
    private Label lblZprava;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    public void setZpravu(String zprava) {
        lblZprava.setText(zprava);
    }
    
}
