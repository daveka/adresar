/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.knotek;

import com.knotek.adrapp.hlavni.view.HlavniOknoController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author david
 */
public class AdresarApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/knotek/adrapp/hlavni/view/hlavni_okno.fxml"));
        Parent root = (Parent) loader.load();
        HlavniOknoController controller = loader.getController(); // přístup ke kontroleru hlavního okna, respektive k jeho veřejným metodám

        primaryStage.setTitle("Adresář");
        primaryStage.setScene(new Scene(root));
        
        // Reakce na stisk křížku a uzavření programu
        primaryStage.setOnCloseRequest(e -> {
            e.consume();
            controller.ukoncitProgram();
        });

        // Minimální velikost okna
        primaryStage.setMinWidth(750.0);
        primaryStage.setMinHeight(250.0);
        primaryStage.setWidth(930.0);
        primaryStage.setHeight(600.0);

        primaryStage.show();
    }

}
