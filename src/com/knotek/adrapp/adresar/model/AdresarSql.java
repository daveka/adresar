package com.knotek.adrapp.adresar.model;

import com.knotek.adrapp.adresar.export.Export;
import com.knotek.adrapp.adresar.export.TypExport;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Abstraktní třída, která se přihlásila k interfacu AdresarData. Jsou zde
 * definované metody pro přístup k databázi založených na SQL. Abstraktní třída
 * může, ale nemusí definovat metody, ke kterým se zavázala. Zodpovědnost za
 * metody, které tato třída nebude definovat, se přenese na třídy, které budou z
 * této abstraktní třídy dědit. Definovány jsou všechny metody, které jsou
 * společné pro práci s SQLITE a MYSQL.
 */
abstract class AdresarSql implements AdresarData {

    private Connection conn;
    private String sqlAutoIncrement;

    /**
     * Konstruktor, který zároveň řeší různý zápis AUTOINCREMENT u SQLite a
     * MySql.
     *
     * @param ai
     */
    public AdresarSql(TypIncrement ai) {
        if (ai == TypIncrement.MYSQL) {
            sqlAutoIncrement = TypIncrement.MYSQL.get();
        }

        if (ai == TypIncrement.SQLITE) {
            sqlAutoIncrement = TypIncrement.SQLITE.get();
        }
    }

    /**
     * Nastavíme spojení.
     *
     * @param conn
     */
    void setConnection(Connection conn) {
        this.conn = conn;
    }

    /**
     * Uzavřeme spojení.
     */
    @Override
    public void close() {
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(AdresarSql.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Vytvoříme tabulku v databázi.
     */
    @Override
    public void setup() {
        try {
            PreparedStatement pre = conn.prepareStatement("CREATE TABLE IF NOT EXISTS osoby ("
                    + "id       INTEGER PRIMARY KEY " + sqlAutoIncrement + " UNIQUE NOT NULL,"
                    + "jmeno    VARCHAR (20) NOT NULL,"
                    + "prijmeni VARCHAR (20) NOT NULL,"
                    + "email    VARCHAR (30),"
                    + "telefon  VARCHAR (20));");

            pre.execute();

        } catch (SQLException ex) {
            Logger.getLogger(AdresarSql.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Nepodařilo se vytvořit tabulku osoby.");
        }
    }

    /**
     * Získáme všechny osoby z databáze.
     *
     * @return seznam osob
     */
    @Override
    public ObservableList<Osoba> ziskatOsoby() {
        ObservableList<Osoba> osoby = FXCollections.observableArrayList();
        ResultSet res = null;

        try {
            PreparedStatement pre = conn.prepareStatement("SELECT * FROM osoby ORDER BY prijmeni;");
            res = pre.executeQuery();

            // Projdeme nalezený seznam osob a vložíme je do kolekce osoby
            while (res.next()) {
                osoby.add(new Osoba(
                        res.getInt("id"),
                        res.getString("jmeno"),
                        res.getString("prijmeni"),
                        res.getString("email"),
                        res.getString("telefon")));
            }
            return osoby;

        } catch (SQLException ex) {
            Logger.getLogger(AdresarSql.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Nepodarilo se ziskat vsechny osoby.");
            return null;
        } finally {
            try {
                if (res != null) {
                    res.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AdresarSql.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Vyhledáme záznamy z databáze podle nějakých kritérií.
     *
     * @param typ po dle čeho budeme hledat (jméno, příjmení, email, telefon)
     * @param coHledat co hledáme.
     * @return seznam nalezených osob
     */
    @Override
    public ObservableList<Osoba> ziskatOsoby(TypHledat typHledat, String coHledat) {
        ObservableList<Osoba> osoby = FXCollections.observableArrayList();
        ResultSet res = null;
        String where = "";
        String hodnota = "";

        // určíme podmínku, kterou pak vložíme do našeho sql dotazu
        switch (typHledat) {
            case JMENO:
                where = "jmeno LIKE ?";
                hodnota = "%" + coHledat + "%";
                break;
            case PRIJMENI:
                where = "prijmeni LIKE ?";
                hodnota = "%" + coHledat + "%";
                break;
            case EMAIL:
                where = "email LIKE ?";
                hodnota = "%" + coHledat + "%";
                break;
            case TELEFON:
                where = "telefon LIKE ?";
                hodnota = "%" + coHledat + "%";
                break;
            default:
                break;
        }

        try {
            PreparedStatement pre = conn.prepareStatement("SELECT * FROM osoby WHERE " + where + " ORDER BY prijmeni;");
            pre.setString(1, hodnota);
            res = pre.executeQuery();

            // Projdeme seznam nalezených osob a vložíme je do kolekce osoby, kterou pak vrátíme
            while (res.next()) {
                osoby.add(new Osoba(res.getInt("id"), res.getString("jmeno"), res.getString("prijmeni"), res.getString("email"), res.getString("telefon")));
            }
            return osoby;

        } catch (SQLException ex) {
            Logger.getLogger(AdresarSql.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            if (res != null) {
                try {
                    res.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AdresarSql.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    /**
     * Vkládáme záznam do databáze
     *
     * @param osoba nová osoba
     * @return true, pokud se vše podaří
     */
    @Override
    public boolean ulozitOsobu(Osoba osoba) {
        try {
            PreparedStatement pre = conn.prepareStatement("INSERT INTO osoby VALUES(NULL,?,?,?,?);");
            pre.setString(1, osoba.getJmeno());
            pre.setString(2, osoba.getPrijmeni());
            pre.setString(3, osoba.getEmail());
            pre.setString(4, osoba.getTelefon());
            pre.executeUpdate();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(AdresarSql.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Nepodarilo se vlozit novou osobu.");
            return false;
        }
    }

    /**
     * Editujeme záznam v databázi.
     *
     * @param id id osoby, která se bude editovat
     * @param osoba nové hodnoty, které nahradí ty stávající
     * @return true, pokud se vše podařilo
     */
    @Override
    public boolean ulozitOsobu(int id, Osoba osoba) {
        try {
            PreparedStatement pre = conn.prepareStatement("UPDATE osoby SET "
                    + "jmeno=?,"
                    + "prijmeni=?,"
                    + "email=?,"
                    + "telefon=? "
                    + "WHERE id=?;");
            pre.setString(1, osoba.getJmeno());
            pre.setString(2, osoba.getPrijmeni());
            pre.setString(3, osoba.getEmail());
            pre.setString(4, osoba.getTelefon());
            pre.setInt(5, id);
            pre.executeUpdate();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(AdresarSql.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Nepodarilo se editace osoby.");
            return false;
        }
    }

    /**
     * Odstraní záznam z databáze.
     *
     * @param id id osoby, která bude odstraněna
     * @return true, pokud se vše podaří
     */
    @Override
    public boolean smazatOsobu(int id) {
        try {
            PreparedStatement pre = conn.prepareStatement("DELETE FROM osoby WHERE id=?;");
            pre.setInt(1, id);
            pre.executeUpdate();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(AdresarSql.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Nepodarilo se smazat osobu.");
            return false;
        }

    }

    /**
     * Export osoby do CSV nebo XML souboru.
     * @param jmenoSouboru název souboru, kam seznam osoby bude exportovat
     * @param typ typ exportu (CSV, XML)
     * @return true, pokud se vše podaří
     */
    @Override
    public boolean exportovatOsoby(String jmenoSouboru, TypExport exportTyp) {
        ResultSet res = null;
        try {
            PreparedStatement pre = conn.prepareStatement("SELECT * FROM osoby ORDER BY prijmeni");
            res = pre.executeQuery();

            List<Osoba> osoby = new ArrayList<>();
            while (res.next()) {
                osoby.add(new Osoba(res.getInt("id"), res.getString("jmeno"), res.getString("prijmeni"), res.getString("email"), res.getString("telefon")));
            }

            if (exportTyp == TypExport.CSV) {
                Export.exportovatDoCSV(osoby, jmenoSouboru);
            }

            if (exportTyp == TypExport.XML) {
                Export.exportovatDoXML(osoby, jmenoSouboru);
            }
            return true;

        } catch (IOException | SQLException ex) {
            Logger.getLogger(AdresarSql.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Nepodaril se export osob.");
            return false;
            
        } finally {
            if (res != null) {
                try {
                    res.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AdresarSql.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /*
     * Priklad vkladani více záznamů najednou. records je kolekce ne seznam dat, ktere je potreba do databaze vlozit
     * ============== Priklad 1 ===============
     * String sql = "INSERT INTO student (studentid, titleid, forename, familyname, dateofbirth) VALUES (?, ?, ?, ?, ?);";
     * 
     * ps = connection.prepareStatement(SQL_INSERT);
     * for (int i = 0; i < records.size(); i++) {
     *     ps.setString(1, records.getName());
     *     //...
     *     ps.addBatch();
     * }
     * ps.executeBatch();
     * 
     * ============== Priklad 2 ================
     * connection.setAutoCommit(false);  
     * PreparedStatement ps = connection.prepareStatement(sql);            
     * for (Record record : records) {
          // etc.
     *    ps.addBatch();
     * }
     * ps.executeBatch();
     * connection.commit(); 
     * 
     */
}
