package com.knotek.adrapp.adresar.model;

/**
 * Výčet autoincrement. Ten je různý u MySql a SQLite databází.
 */
enum TypIncrement {
    SQLITE("AUTOINCREMENT"),
    MYSQL("AUTO_INCREMENT");

    private final String ai;

    private TypIncrement(String ai) {
        this.ai = ai;
    }

    String get() {
        return ai;
    }
}
