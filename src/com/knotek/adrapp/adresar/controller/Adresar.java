package com.knotek.adrapp.adresar.controller;

import com.knotek.adrapp.adresar.export.TypExport;
import com.knotek.adrapp.adresar.model.Osoba;
import javafx.collections.ObservableList;
import com.knotek.adrapp.adresar.model.AdresarData;
import com.knotek.adrapp.adresar.model.TypHledat;

/**
 * Adresář, který umí základní věci, které by adresář měl umět: zjistit osoby,
 * editovat apod. Adresář neumí přistupovat k datům přímo a vlastně ho ani
 * nezajímá, kde jsou data uložena, či jak tam jsou uložena. Podstatné je, že získá
 * to, co potřebuje, ať je zdrojem dat cokoliv. 
 * 
 * Toho je dosaženo díky interface AdresarData, za který se mohou vydávat všechny
 * třídy, které se k němu přihlásily. V našem případě se k němu přihlásily třídy
 * AdresarMysql a AdresarSqlite.
 */
public class Adresar {

    private final AdresarData adresarData;

    /**
     * Konstruktor, který nastaví zdroj dat.
     * @param adresarData interface, za který se mohou vydávat třídy AdresarSqlite a AdresarMysql
     */
    public Adresar(AdresarData adresarData) {
        this.adresarData = adresarData;
        adresarData.connect();
        adresarData.setup();
        adresarData.close();
    }

    /**
     * Získáme všechny osoby.
     * @return seznam osob
     */
    public ObservableList<Osoba> zobrazitVsechnyOsoby() {
        adresarData.connect();
        ObservableList<Osoba> osoby = adresarData.ziskatOsoby();
        adresarData.close();
        return osoby;
    }
    
    /**
     * Vyhledáme osoby
     * @param typ podle čeho budeme hledat (jméno, příjmení, email, telefon)
     * @param coHledat řetězec, která hledáme
     * @return seznam nalezených osob
     */
    public ObservableList<Osoba> vyhledatOsoby(TypHledat typ, String coHledat) {
        adresarData.connect();
        ObservableList<Osoba> osoby = adresarData.ziskatOsoby(typ, coHledat);
        adresarData.close();
        return osoby;
    }

    /**
     * Odstraníme osobu
     * @param id id osoby, kterou budeme chtít odstranit
     * @return true, pokud se vše podaří
     */
    public boolean odstranitOsobu(int id) {
        adresarData.connect();
        boolean ok = adresarData.smazatOsobu(id);
        adresarData.close();
        return ok;
    }

    /**
     * vložíme novou osobu.
     * @param osoba osoba, kterou budeme vkládat
     * @return true, pokud se vše podaří
     */
    public boolean vlozitNovouOsobu(Osoba osoba) {
        adresarData.connect();
        boolean ok = adresarData.ulozitOsobu(osoba);
        adresarData.close();
        return ok;
    }

    /**
     * Editujeme osobu.
     * @param id id osoby, kterou chceme editovat
     * @param osoba nové hodnoty osoby
     * @return true, pokud se vše podaří
     */
    public boolean editovatOsobu(int id, Osoba osoba) {
        adresarData.connect();
        boolean ok = adresarData.ulozitOsobu(id, osoba);
        adresarData.close();
        return ok;
    }
    
    /**
     * Exportovat osobu
     * @param nazevSouboru nazev souboru
     * @param exportTyp typ exportu (csv, xml)
     * @return  tru/false podle toho jak se to podari
     */
    public boolean exportovatOsoby(String nazevSouboru, TypExport exportTyp) {
        adresarData.connect();
        boolean ok = adresarData.exportovatOsoby(nazevSouboru, exportTyp);
        adresarData.close();
        return ok;
    }

}
