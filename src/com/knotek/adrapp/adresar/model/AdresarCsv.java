package com.knotek.adrapp.adresar.model;

import com.knotek.adrapp.adresar.export.Export;
import com.knotek.adrapp.adresar.export.TypExport;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Prace se souborem csv.
 * @author david
 */
public class AdresarCsv implements AdresarData {

    private BufferedReader br;
    private PrintWriter pw;
    private final String souborData;
    private final String souborAutoincrement;
    private int increment;

    public AdresarCsv() {
        souborData = "data.csv";
        souborAutoincrement = "autoincrement";
        increment = 0;
    }

    /**
     * Ziskame vsechny osoby ze souboru. Je to v podstate textovy soubor a tak 
     * k nemu i pristupujeme. Cteme jeddnotlive rakdy, rozdelime jej a nakonec
     * vratime jako seznam.
     * @return seznam osob
     */
    @Override
    public ObservableList<Osoba> ziskatOsoby() {
        ObservableList<Osoba> osoby = FXCollections.observableArrayList();
        try {
            String radek;
            while ((radek = br.readLine()) != null) {
                String hodnoty[] = radek.split("\t", 5);
                osoby.add(new Osoba(Integer.valueOf(hodnoty[0].trim()), hodnoty[1], hodnoty[2], hodnoty[3], hodnoty[4]));
            }

            Collections.sort(osoby, (Osoba o1, Osoba o2) -> o1.getPrijmeni().compareTo(o2.getPrijmeni()));
            return osoby;

        } catch (IOException ex) {
            Logger.getLogger(AdresarCsv.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Nepodarilo se ziskat osoby.");
            return null;
        }
    }

    /**
     * Vyhledani osob.
     * @param typHledat typ hledani
     * @param coHledat co hledame
     * @return seznam osob
     */
    @Override
    public ObservableList<Osoba> ziskatOsoby(TypHledat typHledat, String coHledat) {
        ObservableList<Osoba> osoby = FXCollections.observableArrayList();

        String radek;
        try {
            while ((radek = br.readLine()) != null) {
                String hodnoty[] = radek.split("\t", 5);
                
                switch (typHledat) {
                    case JMENO:
                        if (hodnoty[1].toLowerCase().contains(coHledat.toLowerCase())) {
                            osoby.add(new Osoba(Integer.valueOf(hodnoty[0].trim()), hodnoty[1], hodnoty[2], hodnoty[3], hodnoty[4]));
                        }   break;
                    case PRIJMENI:
                        if (hodnoty[2].toLowerCase().contains(coHledat.toLowerCase())) {
                            osoby.add(new Osoba(Integer.valueOf(hodnoty[0].trim()), hodnoty[1], hodnoty[2], hodnoty[3], hodnoty[4]));
                        }   break;
                    case EMAIL:
                        if (hodnoty[3].toLowerCase().contains(coHledat.toLowerCase())) {
                            osoby.add(new Osoba(Integer.valueOf(hodnoty[0].trim()), hodnoty[1], hodnoty[2], hodnoty[3], hodnoty[4]));
                        }   break;
                    case TELEFON:
                        if (hodnoty[4].toLowerCase().contains(coHledat.toLowerCase())) {
                            osoby.add(new Osoba(Integer.valueOf(hodnoty[0].trim()), hodnoty[1], hodnoty[2], hodnoty[3], hodnoty[4]));
                        }   break;
                    default:
                        break;
                }

            }
            
            // vyhledane osoby seradime podle prijmeni
            Collections.sort(osoby, (Osoba o1, Osoba o2) -> o1.getPrijmeni().compareTo(o2.getPrijmeni()));
            return osoby;

        } catch (IOException ex) {
            Logger.getLogger(AdresarCsv.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Vlozime novou osobu.
     * @param osoba osoba
     * @return
     */
    @Override
    public boolean ulozitOsobu(Osoba osoba) {
        pw.println(String.valueOf(increment)
                + "\t" + osoba.getJmeno()
                + "\t" + osoba.getPrijmeni()
                + "\t" + osoba.getEmail()
                + "\t" + osoba.getTelefon());

        autoincrement();
        return true;
    }

    /**
     * Slouzi k editaci osoby. Ve skutecnosti cte cely soubor, vynecha editovanou
     * a vlozi na konec osobu jako novou.
     * @param id id osoby
     * @param osoba osoba
     * @return 
     */
    @Override
    public boolean ulozitOsobu(int id, Osoba osoba) {
        List<String> osoby = new ArrayList<>();
        String radek;

        try {
            while ((radek = br.readLine()) != null) {
                String hodnoty[] = radek.split("\t", 5);

                if (hodnoty[0].equals(String.valueOf(id))) {
                    continue;
                }
                osoby.add(radek);
            }

            try (PrintWriter pwr = new PrintWriter(new FileWriter(souborData))) {
                osoby.forEach(s -> {
                    pwr.println(s);
                });
                pwr.println(String.valueOf(id)
                        + "\t" + osoba.getJmeno()
                        + "\t" + osoba.getPrijmeni()
                        + "\t" + osoba.getEmail()
                        + "\t" + osoba.getTelefon());
            }
            return true;

        } catch (IOException ex) {
            Logger.getLogger(AdresarCsv.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Odstrani osobu. Ve skutecnosti cte cely soubor, vynecha tu, kterou potrebujeme
     * odstranit a vytvori novy soubor uz bez te, kterou tam nechceme.
     * @param id id osoby
     * @return 
     */
    @Override
    public boolean smazatOsobu(int id) {
        String radek;
        List<String> seznam = new ArrayList<>();
        try {
            while ((radek = br.readLine()) != null) {
                String hodnoty[] = radek.split("\t", 5);

                if (hodnoty[0].equals(String.valueOf(id))) {
                    continue;
                }
                seznam.add(radek);
            }

            try (PrintWriter pwr = new PrintWriter(new FileWriter(souborData))) {
                seznam.forEach((s) -> pwr.println(s));
            }
            return true;

        } catch (IOException ex) {
            Logger.getLogger(AdresarCsv.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Exportujeme osoby bud do csv, nebo do xml souboru.
     * @param jmenoSouboru nazev souboru
     * @param exportTyp typ exportu
     * @return 
     */
    @Override
    public boolean exportovatOsoby(String jmenoSouboru, TypExport exportTyp) {
        try {

            List<Osoba> osoby = new ArrayList<>();

            // nacteme cely soubor
            String radek;
            while ((radek = br.readLine()) != null) {
                String hodnoty[] = radek.split("\t", 5);
                osoby.add(new Osoba(Integer.valueOf(hodnoty[0]), hodnoty[1], hodnoty[2], hodnoty[3], hodnoty[4]));
            }

            // exportujeme
            if (exportTyp == TypExport.CSV) {
                Export.exportovatDoCSV(osoby, jmenoSouboru);
            }

            if (exportTyp == TypExport.XML) {
                Export.exportovatDoXML(osoby, jmenoSouboru);
            }
            return true;

        } catch (IOException ex) {
            Logger.getLogger(AdresarCsv.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    /**
     * Pokud soubor, kam se budou ukladat osoby neexistuje, vytvorime jej.
     * Vytvorime spojeni na data. Do souboru autoincrement ukladam id posledni vlozene osoby.
     */
    @Override
    public void connect() {
        try {
            if (!Files.exists(Paths.get(souborData))) {
                Files.createFile(Paths.get(souborData));
                autoincrement();
            }

            br = new BufferedReader(new FileReader(souborData));
            pw = new PrintWriter(new FileWriter(souborData, true));

        } catch (FileNotFoundException ex) {
            Logger.getLogger(AdresarCsv.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Nepodarilo se otevrit svc soubor.");
        } catch (IOException ex) {
            Logger.getLogger(AdresarCsv.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Nepodarilo se otevrit soubor pro zapis.");
        }
    }

    /**
     * Uzavreme soubory.
     */
    @Override
    public void close() {
        try {
            if (br != null) {
                br.close();
            }

            if (pw != null) {
                pw.close();
            }

        } catch (IOException ex) {
            Logger.getLogger(AdresarCsv.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Nacteme id ze souboru autoincrement.
     */
    @Override
    public void setup() {
        try (BufferedReader iod = new BufferedReader(new FileReader(souborAutoincrement))) {
            increment = Integer.valueOf(iod.readLine());

        } catch (FileNotFoundException ex) {
            Logger.getLogger(AdresarCsv.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AdresarCsv.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda zvysuje id o jedna a uklada je do souboru.
     */
    private void autoincrement() {
        try (PrintWriter p = new PrintWriter(new FileWriter(souborAutoincrement))) {
            p.println(String.valueOf(++increment));

        } catch (IOException ex) {
            Logger.getLogger(AdresarCsv.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
