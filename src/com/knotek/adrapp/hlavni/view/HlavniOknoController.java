package com.knotek.adrapp.hlavni.view;

import com.knotek.adrapp.adresar.view.AdresarController;
import com.knotek.adrapp.adresar.view.ExportovatController;
import com.knotek.adrapp.faktury.FakturyController;
import com.knotek.adrapp.util.AlertBox;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class HlavniOknoController implements Initializable {

    private Parent paAdresar, paFaktury; // budou uloženy okna: adresar.fxml a faktury.fxml
    private FakturyController fakturyController; // kontroler okna faktury.fxml
    private AdresarController adresarController; // kontroler okna adresar.fxml

    @FXML
    private BorderPane bpHlavniOkno;

    @FXML
    private Label lblTypDatabaze;

    /**
     * Reakce na stisk tlačítka Adresar v levém sloupci
     *
     * @param event
     */
    @FXML
    private void onZobrazitAdresar(ActionEvent event) {
        zobrazitOkno(paAdresar);
    }

    /**
     * Reakce na stisk tlačítka Faktury v levém sloupci
     *
     * @param event
     */
    @FXML
    private void onZobrazitFaktury(ActionEvent event) {
        zobrazitOkno(paFaktury);
    }

    /**
     * Reakce na stisk tlačítka menu Ukoncit
     *
     * @param event
     */
    @FXML
    private void onUkoncit(ActionEvent event) {
        ukoncitProgram();
    }

    /**
     * Reakce na stisk tlačítka menu Prepnout na Sqlite
     *
     * @param event
     */
    @FXML
    private void onPrepnoutNaSqlite(ActionEvent event) {
        adresarController.prepnoutNaSqlite();
        lblTypDatabaze.setText(adresarController.getTypDatabaze());
    }

    /**
     * Reakce na stisk tlačítka menu Prepnout na Mysql
     *
     * @param event
     */
    @FXML
    private void onPrepnoutNaMysql(ActionEvent event) {
        adresarController.prepnoutNaMysql();
        lblTypDatabaze.setText(adresarController.getTypDatabaze());
    }

    /**
     * Reakce na stisk tlačítka menu Prepnout na CSV
     *
     * @param event
     */
    @FXML
    private void onPrepnoutNaCsv(ActionEvent event) {
        adresarController.prepnoutNaCsv();
        lblTypDatabaze.setText(adresarController.getTypDatabaze());
    }

    /**
     * Reakce na stisk tlačítka menu Zobrazit zpravu cislo dve
     *
     * @param event
     */
    @FXML
    private void onZobrazitZpravu2(ActionEvent event) {
        fakturyController.setZpravu("Tohle je zpráva 2...");
    }

    /**
     * Reakce na stisk tlačítka menu Zobrazit zpravu cislo jedna
     *
     * @param event
     */
    @FXML
    private void onZobrazitZpravu1(ActionEvent event) {
        fakturyController.setZpravu("Tohle je zpráva 1...");
    }

    @FXML
    private void onOProgramu(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/com/knotek/adrapp/hlavni/view/o_programu.fxml"));
            Stage stage = new Stage();
            stage.setTitle("O programu");

            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner((Stage) bpHlavniOkno.getScene().getWindow());

            stage.setMinWidth(600.0);
            stage.setMaxWidth(600.0);
            stage.setMinHeight(400.0);
            stage.setMaxHeight(400.0);

            stage.setScene(new Scene(root));
            stage.showAndWait();
        } catch (IOException ex) {
            Logger.getLogger(HlavniOknoController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Reakce na stisk tlačítka menu Exportovat do...
     *
     * @param event
     */
    @FXML
    private void onExportovatOsoby(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/knotek/adrapp/adresar/view/exportovat.fxml"));
            Parent root = loader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));

            ExportovatController controller = loader.getController();

            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner((Stage) bpHlavniOkno.getScene().getWindow());

            stage.setTitle("Exportovat");
            stage.showAndWait();

            if (controller.isOK()) {
                adresarController.exportovatOsoby(controller.ziskatNazevSouboru(), controller.vybranyTyp());
            }

        } catch (IOException ex) {
            Logger.getLogger(HlavniOknoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            FXMLLoader loaderAdresar = new FXMLLoader(getClass().getResource("/com/knotek/adrapp/adresar/view/adresar.fxml"));
            paAdresar = (Parent) loaderAdresar.load();

            FXMLLoader loaderFaktury = new FXMLLoader(getClass().getResource("/com/knotek/adrapp/faktury/faktury.fxml"));
            paFaktury = (Parent) loaderFaktury.load();

            fakturyController = loaderFaktury.getController();
            adresarController = loaderAdresar.getController();

            lblTypDatabaze.setText(adresarController.getTypDatabaze());

            zobrazitOkno(paAdresar);

        } catch (IOException ex) {
            Logger.getLogger(HlavniOknoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Zobrazíme okno - slouží k přepínání oken.
     *
     * @param pa
     */
    private void zobrazitOkno(Parent pa) {
        bpHlavniOkno.setCenter(pa);
    }

    /**
     * Ukončení programu.
     */
    public void ukoncitProgram() {
        if (AlertBox.displayYN("Ukončit program", "Opravdu chcete ukončit program?", "", bpHlavniOkno)) {
            System.out.println("Zavírám program...");
            Stage stage = (Stage) bpHlavniOkno.getScene().getWindow();
            System.out.println("Velikost okna:" + stage.getWidth() + "x" + stage.getHeight());
            stage.close();
            //Platform.exit();
        }
    }

}
