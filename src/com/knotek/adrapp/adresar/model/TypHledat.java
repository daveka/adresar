/**
 * Podle čeho se bude hledat v databázi. Řetězce se vloží do comboboxu.
 */
package com.knotek.adrapp.adresar.model;

public enum TypHledat {
    JMENO("Jméno"), PRIJMENI("Příjmení"), EMAIL("Email"), TELEFON("Telefon");
    String typ;

    private TypHledat(String typ) {
        this.typ = typ;
    }

    public String get() {
        return typ;
    }

}
