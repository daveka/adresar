package com.knotek.adrapp.adresar.view;

import com.knotek.adrapp.adresar.model.Osoba;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class Okno, které nám pomůže vkládat, nebo editovat osoby.
 */
public class NovaOsobaController implements Initializable {

    private boolean stisknutoOK;
    private Osoba osoba;

    @FXML
    private Label lblJmeno;

    @FXML
    private Label lblPrijmeni;

    @FXML
    private TextField tfJmeno;

    @FXML
    private TextField tfPrijmeni;

    @FXML
    private TextField tfEmail;

    @FXML
    private TextField tfTelefon;

    @FXML
    private Button btnZrusit;

    @FXML
    private Button btnUlozit;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    /**
     * Stisk tlačítka Uložit. Testujeme, jestli jsme zadali alespoň jméno a
     * příjmení.
     *
     * @param event
     */
    @FXML
    private void onUlozit(ActionEvent event) {
        if (zadanoSpravne()) {
            stisknutoOK = true;
            osoba = new Osoba(0, tfJmeno.getText(), tfPrijmeni.getText(), tfEmail.getText(), tfTelefon.getText());
            ((Stage) btnUlozit.getScene().getWindow()).close();
        }
    }

    /**
     * Stisk tlačítka Zrušit.
     *
     * @param event
     */
    @FXML
    private void onZrusit(ActionEvent event) {
        stisknutoOK = false;
        ((Stage) btnZrusit.getScene().getWindow()).close();
    }

    /**
     * Zjišťujeme jaké tlačítko jsme stiskli
     *
     * @return
     */
    public boolean isOK() {
        return stisknutoOK;
    }

    /**
     * Získáme nově zadanou osobu.
     *
     * @return osoba, kterou jsme zadali
     */
    public Osoba getOsoba() {
        return osoba;
    }

    /**
     * Nastavíme osobu a vložíme ji do políček okna.
     *
     * @param osoba získaná osoba
     */
    public void setOsoba(Osoba osoba) {
        this.osoba = osoba;
        tfJmeno.setText(osoba.getJmeno());
        tfPrijmeni.setText(osoba.getPrijmeni());
        tfEmail.setText(osoba.getEmail());
        tfTelefon.setText(osoba.getTelefon());
    }

    /**
     * Testujeme, jestli jsme zadali alespoň jméno a příjmení. Pokud ne,
     * upozorníme na to červenou barvou.
     *
     * @return true, pokud je vše zadáno správně
     */
    private boolean zadanoSpravne() {
        // Předpokládáme, že jsme vše zadali správně
        boolean zadanoSpravne = true;
        lblJmeno.setStyle("-fx-text-fill: black;");
        lblPrijmeni.setStyle("-fx-text-fill: black;");

        // Testujeme jméno, jestli je náš předpoklad správný. Pokud ne, nastavíme text červeně
        if (tfJmeno.getText().trim().equals("")) {
            lblJmeno.setStyle("-fx-text-fill: red;");
            zadanoSpravne = false;
        } else {
            lblJmeno.setStyle("-fx-text-fill: black;");
        }

        // Testujeme příjmení, jestli je náš předpoklad správný. Pokud ne, nastavíme text červeně
        if (tfPrijmeni.getText().trim().equals("")) {
            lblPrijmeni.setStyle("-fx-text-fill: red;");
            zadanoSpravne = false;
        } else {
            lblPrijmeni.setStyle("-fx-text-fill: black;");
        }
        return zadanoSpravne;
    }
}
