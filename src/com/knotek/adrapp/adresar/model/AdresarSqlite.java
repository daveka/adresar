package com.knotek.adrapp.adresar.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Třída je přímým potomkem abstraktní třídy AdresarSql, a dědí vše, včetně přihlášení se 
 * k interface AdresarData. Abstraktní třída AdresarSql nedefinovala jednu metodu (connect()),
 * která musí být definována zde.
 */
public class AdresarSqlite extends AdresarSql {

    /**
     * Konstruktor, který volá konstruktor třídy svého rodiče a říká mu, 
     * jaký zápis AUTOINCREMENT má použít.
     */
    public AdresarSqlite() {
        super(TypIncrement.SQLITE);
    }

    /**
     * Pripojit k databazi sqlite.
     */
    @Override
    public void connect() {
        Connection conn;
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:data.db");
            setConnection(conn);
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AdresarSql.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Nepodarilo se pripojit k databazi.");
        }
    }
}
