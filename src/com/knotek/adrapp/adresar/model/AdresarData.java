package com.knotek.adrapp.adresar.model;

import com.knotek.adrapp.adresar.export.TypExport;
import javafx.collections.ObservableList;

/**
 * Interface rozsiruje DataConnection. Souvisi s adresarem.
 */
public interface AdresarData extends DataConnection {

    ObservableList<Osoba> ziskatOsoby();

    ObservableList<Osoba> ziskatOsoby(TypHledat typHledat, String coHledat);

    boolean ulozitOsobu(Osoba osoba);

    boolean ulozitOsobu(int id, Osoba osoba);

    boolean smazatOsobu(int id);

    boolean exportovatOsoby(String jmenoSouboru, TypExport exportTyp);
}
