package com.knotek.adrapp.adresar.view;

import com.knotek.adrapp.adresar.export.TypExport;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ExportovatController implements Initializable {

    private boolean vybranoOK;

    @FXML
    private ComboBox<String> cbExportTyp;

    @FXML
    private TextField tfSoubor;

    @FXML
    private Label lblZprava;

    /**
     * Reakce na stisk tlacitka vybrat soubor.
     * @param event 
     */
    @FXML
    void onVybratSoubor(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Exportovat soubor");
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        File file = fileChooser.showSaveDialog(stage);

        if (file != null) {
            tfSoubor.setText(file.getPath());
        }

    }

    /**
     * Reakce na stisk tlacitka Exportovat.
     * @param event 
     */
    @FXML
    void onExportovat(ActionEvent event) {
        if (!tfSoubor.getText().trim().equals("")) {
            vybranoOK = true;
            ((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
        } else {
            lblZprava.setText("Nejdříve vyberte soubor!");
        }

    }

    /**
     * Stisk tlacitka zrusit.
     * @param event 
     */
    @FXML
    void onZrusit(ActionEvent event) {
        vybranoOK = false;
        ((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cbExportTyp.getItems().setAll(TypExport.CSV.get(), TypExport.XML.get());
        cbExportTyp.getSelectionModel().selectFirst();
    }

    /**
     * Vybrano tlacitko OK
     * @return 
     */
    public boolean isOK() {
        return vybranoOK;
    }

    /**
     * Ziskame nazev souboru
     * @return 
     */
    public String ziskatNazevSouboru() {
        return tfSoubor.getText();
    }

    /**
     * Co jsme vybrali z rozbalovaciho menu.
     * @return 
     */
    public TypExport vybranyTyp() {
        TypExport et = TypExport.CSV;
        if (cbExportTyp.getSelectionModel().getSelectedItem().equals(TypExport.CSV.get())) {
            et = TypExport.CSV;
        }
        if (cbExportTyp.getSelectionModel().getSelectedItem().equals(TypExport.XML.get())) {
            et = TypExport.XML;
        }
        return et;
    }

}
