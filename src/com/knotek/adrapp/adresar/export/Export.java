package com.knotek.adrapp.adresar.export;

import com.knotek.adrapp.adresar.model.Osoba;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class Export {

    /**
     * Exportovat cely adresar do CSV
     * @param osoby seznam osob
     * @param jmenoSouboru nazev souboru
     * @throws IOException 
     */
    public static void exportovatDoCSV(List<Osoba> osoby, String jmenoSouboru) throws IOException {
        try ( PrintWriter pw = new PrintWriter(new FileWriter(jmenoSouboru))) {
            pw.println("ID\tjmeno\tprijmeni\temail\ttelefon");

            // seradime osoby podle prijmeni
            Collections.sort(osoby, (Osoba o1, Osoba o2) -> o1.getPrijmeni().compareTo(o2.getPrijmeni()));

            // ulozime do souboru
            osoby.forEach(osoba -> {
                pw.println(String.valueOf(osoba.getId())
                        + "\t" + osoba.getJmeno()
                        + "\t" + osoba.getPrijmeni()
                        + "\t" + osoba.getEmail()
                        + "\t" + osoba.getTelefon());
            });
        }
    }

    /**
     * Exportovat cely adresar do XML
     * @param osoby seznam osob
     * @param jmenoSouboru nazev souboru
     */
    public static void exportovatDoXML(List<Osoba> osoby, String jmenoSouboru) {
        try {
            Collections.sort(osoby, (Osoba o1, Osoba o2) -> o1.getPrijmeni().compareTo(o2.getPrijmeni()));
            XMLOutputFactory xmlof = XMLOutputFactory.newInstance();

            XMLStreamWriter writer = xmlof.createXMLStreamWriter(new FileOutputStream(jmenoSouboru));
            writer.writeStartDocument();
            writer.writeStartElement("osoby");

            osoby.forEach(osoba -> {
                try {
                    writer.writeStartElement("osoba");
                    writer.writeAttribute("id", String.valueOf(osoba.getId()));

                    writer.writeStartElement("jmeno");
                    writer.writeCharacters(osoba.getJmeno());
                    writer.writeEndElement();

                    writer.writeStartElement("prijmeni");
                    writer.writeCharacters(osoba.getPrijmeni());
                    writer.writeEndElement();

                    writer.writeStartElement("email");
                    writer.writeCharacters(osoba.getEmail());
                    writer.writeEndElement();

                    writer.writeStartElement("telefon");
                    writer.writeCharacters(osoba.getTelefon());
                    writer.writeEndElement();

                    writer.writeEndElement();
                } catch (XMLStreamException ex) {
                    Logger.getLogger(Export.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            writer.writeEndElement();
            writer.writeEndDocument();
            writer.flush();
            writer.close();

        } catch (XMLStreamException | IOException ex) {
            Logger.getLogger(Export.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
