package com.knotek.adrapp.adresar.model;

/**
 * Interface - hlavni metody k pristupu do databaze.
 * @author david
 */
public interface DataConnection {

    public void connect();

    public void close();

    public void setup();
}
